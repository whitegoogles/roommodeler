/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
	// Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
		//Ok, lets try setting up the gyros using this:  http://www.html5rocks.com/en/tutorials/device/orientation/
		if(window.DeviceOrientationEvent){
			window.addEventListener('deviceorientation',gyroHandler);
		}
		function gyroHandler(gyroSensor){
			var lrRot = gyroSensor.gamma; //This is left to right tilt
			var fbRot = gyroSensor.beta;  //This is front to back tilt
			var compassRot = gyroSensor.alpha; //?This is compass direction
			document.getElementById('lrTilt').innerHTML = ""+Math.round(lrRot*10000)/10000;
			document.getElementById('fbTilt').innerHTML = ""+Math.round(fbRot*10000)/10000;
			document.getElementById('compass').innerHTML = ""+Math.round(compassRot*10000)/10000;
			if(storeData){
				gyro.push(gyroSensor);
			}
		}
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
		//Set up the collect data button
		document.getElementById("collectDataBtn").addEventListener('click',collectData,false);
        app.receivedEvent('deviceready');
		navigator.accelerometer.watchAcceleration(
			success,error,{frequency:50}
		);
		var sensorSteps = 	['SET_ACCEL',
							 'SET_JERK',
							 'SET_DER_ACCEL',
							 'SET_VEL',
							 'SET_POS'];
		var sensorStep = 0;
		//Timestamps are in uptime since the android device turned on (in nanoseconds).
		var timeStamp,lastTimeStamp,lastAccel,lastVel,pos,jerk,accel,vel,pos,lastDerAccel,derAccel;
		derAccel = lastDerAccel = vel = lastVel = pos = 0;
		function success(acceleration){
			var storeStamp = acceleration.timestamp;
			acceleration.timestamp = acceleration.timestamp/1000; //Literally why is there so much misinfo on this 000; //Lets try something a little smaller //000;//Divide by a billion to scale from nanoseconds to seconds
			if(timeStamp !== acceleration.timestamp && (typeof timeStamp ==='undefined' || acceleration.timestamp-timeStamp>0.05)){ //Idk why this happens, probably too fast of a sample rate?
				//acceleration.timestamp
				//acceleration.x
				function setAccel(){
					timeStamp = acceleration.timestamp; 
					accel = acceleration.x;
				}
				function setJerk(){
					lastTimeStamp = timeStamp;
					lastAccel = accel;
					setAccel();
					jerk = (accel - lastAccel)/(timeStamp - lastTimeStamp);
				}
				function setDerAccel(){
					lastJerk = jerk;
					setJerk();
					var timeDelta = timeStamp - lastTimeStamp;
					derAccel += ((lastJerk + jerk)/2.0)*timeDelta;
				}
				function setVel(){
					lastDerAccel = derAccel;
					setDerAccel();
					var timeDelta = timeStamp - lastTimeStamp;
					vel += ((lastAccel+accel)/2.0) *timeDelta;
				}
				function setPos(){
					lastVel = vel;
					setVel();
					var timeDelta = timeStamp - lastTimeStamp;
					pos += ((lastVel+vel)/2.0)*timeDelta;
				}
				//probably introduce some kind of cap reset, but ignore that for now
				var accelMinCap = 0.05; //Almost guaranteed has to be order lower
				if(accel !== 'undefined' && lastAccel !== 'undefined'){
					if(Math.abs(accel - lastAccel) < accelMinCap){
						sensorStep = 0;
						derAccel = lastDerAccel = 0; //I should definitely zero out acceleration.
						vel = lastVel = 0;//But this could continue at a constant velocity???			
					}
				}
				switch(sensorSteps[sensorStep]){
					case 'SET_ACCEL':
						setAccel();
						sensorStep+=1;
						break;
					case 'SET_JERK':
						setJerk();
						sensorStep+=1;
						break;
					case 'SET_DER_ACCEL':
						setDerAccel();
						sensorStep+=1;
						break;
					case 'SET_VEL':
						setVel();
						sensorStep+=1;
						break;
					case 'SET_POS':
						setPos();
						break;
				}
				document.getElementById('jerkVal').innerHTML = ""+Math.round(jerk*10000)/10000;
				document.getElementById('accelerationVal').innerHTML = ""+Math.round(derAccel*10000)/10000;
				document.getElementById('velocityVal').innerHTML = ""+Math.round(vel*10000)/10000;
				document.getElementById('positionVal').innerHTML = ""+Math.round(pos*10000)/10000;
				document.getElementById('realAccelVal').innerHTML = ""+Math.round(accel*10000)/10000;
			}
			if(storeData){
				copyAccel = {x:acceleration.x,y:acceleration.y,z:acceleration.z,timestamp:storeStamp/1000};
				acc.push(copyAccel);
			}
			acceleration.timestamp*=1000; //Seriously???
		}
		function error(){
			alert('Error reading acceleration');
		}
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        /*var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);*/
    }
};

app.initialize();
var storeData = false;
var acc = [];
var gyro = [];
var oldCollectName = "";
var videoPath = "";
function collectData(){
	if(storeData){
		
		//Stop collecting data and email it out
		console.log("Ok, lets send this out");
		storeData = false;
		
		//make a whole string, call writer. write, then email it
		var i,accStr,gyroStr;
		accStr = "x,y,z,timestamp"+'\n';
		for(i = 0; i<acc.length; i=i+1){
			accStr += acc[i].x+","+acc[i].y+","+acc[i].z+","+acc[i].timestamp+'\n';
		}
		gyroStr = "alpha,gamma,beta,timestamp"+'\n';
		for(i = 0; i<gyro.length; i=i+1){
			gyroStr += gyro[i].alpha+","+gyro[i].gamma+","+gyro[i].beta+","+gyro[i].timestamp+'\n';
		}
		
		//Ok, because of the file limit I need to use oauth here to sign in, and 
		//then we can upload everything to a new directory in google drive
		//Implicit is better because the secret isn't actually stored in the code,
		//so here we go. Implicit basically logs in, gets a token, and then sends
		//that token over to google to get a secret.
		//Thanks to http://phonegap-tips.com/articles/google-api-oauth-with-phonegaps-inappbrowser.html
		//for the example code (I was struggling making any of the plugins work)
		var base_url = 'https://accounts.google.com/o/oauth2/auth?';
		var client_id = '250313483809-vvkhn27fjfg749bam75284ksort0ggk9.apps.googleusercontent.com';
		var redirect = 'http://localhost/callback';
		var client_secret = 'qUc38G11t2RVj1fdSVnCuR9S'; //This isn't great storing this here...
		var auth_url = base_url+$.param({
			client_id: client_id,
			redirect_uri: redirect,
			response_type: 'code',
			scope: "https://www.googleapis.com/auth/drive"
		});
		
		var authWindow = window.open(auth_url,'_blank','location=no,toolbar=no');
		//The jquery.on method doesn't work here
		authWindow.addEventListener('loadstart',function(event){
			var urlParts = event.url.split("?code=");
			if(urlParts.length>1){
				var authCode = urlParts[1];
				authWindow.close();
				$.ajax({
					type: 'POST',
					url:'https://accounts.google.com/o/oauth2/token',
					data: {
						code: authCode,
						client_id:client_id,
						client_secret:client_secret,
						redirect_uri:redirect,
						grant_type: 'authorization_code'
					},
					success: function onSuccess(token){
						var accToken = token.access_token;
						//Okay, lets put everything on the drive
						//  "mimeType": "application/vnd.google-apps.folder"
						gapi.auth.setToken(accToken);
						gapi.client.load('drive','v2',onGapiLoad);
						function onGapiLoad(){
							var request = gapi.client.request({
								'path':'/drive/v2/files',
								'method':'POST',
								'headers':{
									'Content-Type':'application/json',
									'Authorization': 'Bearer '+accToken
								},
								'body':{
									'title': (new Date()).toString(),
									'mimeType': 'application/vnd.google-apps.folder'
								}								
							});
							request.execute(function(resp){
								var folderId = resp.id;
								insertFile(folderId,accStr,'Accelerometer.csv','text/csv','8bit',accStr.length,function(resp){
									insertFile(folderId,gyroStr,'Gyro.csv','text/csv','8bit',accStr.length,function(resp){
										var getResumableUrl = gapi.client.request({
											'path':'/upload/drive/v2/files',
											'method':'POST',
											'params': {'uploadType':'resumable','convert':true},
											'headers':{
												'Authorization': 'Bearer '+accToken,
												'X-Upload-Content-Type': 'video/mp4',
												'X-Upload-Content-Transfer-Encoding': 'base64'
											},
											'body': {
												'title': 'Room.mp4',
												'mimeType': 'video/mp4',
												'parents': [{'id':folderId}]
											}
										});
										getResumableUrl.execute(function(raw,resp){
											console.log(raw);
											console.log(resp);
											var respObj = JSON.parse(resp);
											var locationUrl = respObj.gapiRequest.data.headers.location;
											videoPath.fullPath = videoPath.fullPath.replace('file:///','filesystem:///');
											var vidFile = window.resolveLocalFileSystemURL(videoPath.fullPath,function(fileEntry){
												fileEntry.file(function(file){
													var blockSize =1048576; 
													var curBlock = 0;
													var googleBlock = 0;
													sendChunk();
													function sendChunk(){
														console.log('sending a chunk');
														console.log('file size'+file.size);
														var slicedFile = file.slice(curBlock,(curBlock+blockSize<file.size)? curBlock+blockSize : file.size);
														var reader = new FileReader();
														reader.onloadend = function(e){
															//I literally cannot print out anything about e.target.result,
															//but it definitely exists and is the right length...
															var stripData = e.target.result;
															stripData = stripData.split('base64,')[1]; //Seriously, the data url had all this random crap tacked on the beginning that
																									   //Made google just randomly reject it :(
															console.log('actual sliced size '+stripData.length);
															console.log('content range header '+'bytes '+googleBlock+'-'+((googleBlock+blockSize-1<file.size)?googleBlock+blockSize-1:file.size-1)+'/'+(file.size));
															var request = gapi.client.request({
																'path':locationUrl,
																'method':'PUT',
																'headers':{
																	'Authorization': 'Bearer '+accToken,
																	'Content-Type':'video/mp4',
																	'Content-Encoding':'base64',
																	'Content-Transfer-Encoding':'base64',
																	'Content-Length':stripData.length,
																	'Content-Range': 'bytes '+googleBlock+'-'+((googleBlock+blockSize-1<file.size)?googleBlock+blockSize-1:file.size-1)+'/'+(file.size)
																},
																'body':stripData
															});
															request.execute(function(resp,raw){
																console.log(JSON.stringify(resp));
																console.log(raw);
																googleBlock = parseInt(JSON.parse(raw).gapiRequest.data.headers.range.split('-')[1])+1;
																if(curBlock+blockSize<file.size){
																	curBlock+=blockSize;
																	sendChunk();
																}
															});															
														};
														reader.onerror = function(err){
															console.log(err);
															console.log(JSON.stringify(err));
														};
														reader.readAsDataURL(slicedFile);
													}
												});
											},function(msg){
												console.log(msg);
											});
										});
									});
								});
								function insertFile(folderId,fileData,filename,fileType,transferEncoding,length,callback){
									//I'm not entirely sure what these are for, apparently
									//they're ways for it to know that it's receiving a new multipart packet
									//or that its the end of the whole stream of the file.
									var boundary = btoa('-------314159265358979323846');
									var delimiter = '\r\n--'+boundary+'\r\n';
									var close_delim = '\r\n--'+boundary+'--';
									var reqBody = delimiter+
									'Content-Type: application/json\r\n\r\n'+
									JSON.stringify({
										'title': filename,
										'mimeType': fileType,
										'parents': [{'id':folderId}]
									})+delimiter+
									'Content-Type: '+fileType+'\r\n'+
									'Content-Transfer-Encoding: '+transferEncoding+'\r\n'+'\r\n'+
									fileData+
									close_delim;
									var request = gapi.client.request({
										'path':'/upload/drive/v2/files',
										'method':'POST',
										'params': {'uploadType':'multipart',
												   'convert':true},
										'headers':{
											'Authorization': 'Bearer '+accToken,
											'Content-Type':'multipart/mixed; boundary="'+boundary+'"',
											'Content-Length':length
										},
										'body':reqBody
									});
									request.execute(function(resp){
										callback(resp);
									});
								}
							});
						}
					},
					error: function onError(err){
						console.log("There was some kind of error with getting the access token");
						console.log(JSON.stringify(err));
					}
				});
			}
		});
		//Email the file out
		/*cordova.plugins.email.open({
			to: 'RoomModeler@gmail.com',
			attachments: ["base64:accelerometer"+(new Date())+".csv//"+btoa(accStr),
						  "base64:gyroscope"+(new Date())+".csv//"+btoa(gyroStr)],
			subject: "Sensor Data Collected on "+ (new Date())
		});*/
		
		document.getElementById('collectDataBtn').innerHTML = oldCollectName;
	}
	else{
		storeData = true;
		acc = []; //Clear out any old values;
		gyro = [];
		oldCollectName = document.getElementById('collectDataBtn').innerHTML;
		document.getElementById("collectDataBtn").innerHTML = 'Stop Collecting Data';
		navigator.device.capture.captureVideo(captureSuccess,captureError);
		function captureSuccess(mediaFiles){ //There should only be one of these
			videoPath = mediaFiles[0];
			collectData();
		}
		function captureError(){
			collectData();
			console.log("Camera video capture failed for some reason");
		}
	}
}